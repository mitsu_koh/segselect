package titech.segselect.gui;

import controlP5.CheckBox;
import controlP5.ControlEvent;
import controlP5.ControlListener;
import controlP5.ControlP5;
import titech.segselect.SegSelect;
import titech.segselect.display.CenteredTextDisplay;
import titech.segselect.log.AnnotationLogger;
import titech.segselect.util.ControlP5Creator;
import titech.segselect.util.StdLogger;

public class IntermediateSketch extends Sketch {
    private int documentIndex;
    private int allDocumentNumber;
    private String text;
    private CenteredTextDisplay textBox;

    private ControlP5 cp5;
    private CheckBox wrongOperationSingleCheckBox;

    private boolean isWrongOperation;

    private boolean isFirstSketch;

    private boolean debugTriggerA;
    private boolean debugTriggerB;

    public IntermediateSketch(SegSelect parent, int documentIndex,
            int allDocumentNumber, AnnotationLogger logger) {
        super(parent);

        allDocumentNumber--;

        this.documentIndex = documentIndex;
        this.allDocumentNumber = allDocumentNumber;
        if (documentIndex <= allDocumentNumber) {
            int next = this.documentIndex + 1;
            int total = this.allDocumentNumber + 1;
            this.text = next + "/" + total;
        } else {
            this.text = "終了です";
        }
        this.textBox = new CenteredTextDisplay(parent, text, parent.width / 2 , parent.height / 2);

        if (logger != null) {
            logger.log(this.textBox);
        }

        cp5 = null;
        wrongOperationSingleCheckBox = null;

        isWrongOperation = false;

        isFirstSketch = (documentIndex == 0);

        this.debugTriggerA = false;
        this.debugTriggerB = false;
    }

    @Override
    public void init() {
        super.init();

        cp5 = parent.getCP5();

        if (!isFirstSketch) {
            this.createGUI();
        }

        this.paint();
    }

    @Override
    public void draw() {
        super.draw();

        this.paint();

        textBox.updateMouseState();

        // Debug code
        if (parent.isClicked()) {
            if (parent.mouseX < 100 && parent.mouseY < 100) {
                System.out.println("a");
                debugTriggerA = true;
            } else if (parent.mouseX > 1180 && parent.mouseY > 924) {
                System.out.println("b");
                debugTriggerB = true;
            }
            if (debugTriggerA && debugTriggerB) {
                parent.debug = !parent.debug;
                debugTriggerA = false;
                debugTriggerB = false;
            }
        }
    }

    @Override
    public void stop() {
        super.stop();

        if (isWrongOperation) {
            int pDocumentIndex = documentIndex - 1;
            AnnotationLogger.logWrongOperation(String.valueOf(pDocumentIndex));
        }

        this.destroyGUI();
    }

    @Override
    public void paint() {
        super.paint();

        parent.background(255);
        textBox.draw();
    }

    private void createGUI() {
        wrongOperationSingleCheckBox = ControlP5Creator.createSingleCheckBox(cp5,
                "間違えた",
                600, 800, 20, 20);

        wrongOperationSingleCheckBox.getItem(0).addListener(new ControlListener() {
            @Override
            public void controlEvent(ControlEvent ce) {
                if (ce.isFrom(wrongOperationSingleCheckBox.getItem(0))) {
                    isWrongOperation =
                            wrongOperationSingleCheckBox.getItem(0)
                            .getValue() != 0;
                    StdLogger.getInstance().info(
                            "isWrongOperation: " + isWrongOperation);
                }
            }
        });
    }

    private void destroyGUI() {
        if (wrongOperationSingleCheckBox != null) {
            wrongOperationSingleCheckBox.remove();
        }
    }
}