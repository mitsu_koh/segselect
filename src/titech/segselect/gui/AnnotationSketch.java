package titech.segselect.gui;

import titech.segselect.SegSelect;
import titech.segselect.annotation.Document;
import titech.segselect.display.DocumentDisplay;
import titech.segselect.log.AnnotationLogger;

public class AnnotationSketch extends Sketch {
    private DocumentDisplay document;
    private AnnotationLogger logger;

    public AnnotationSketch(SegSelect parent,
            Document document, AnnotationLogger logger) {
        super(parent);

        this.document = new DocumentDisplay(parent, document);
        this.logger = logger;
    }

    @Override
    public void init() {
        super.init();

        updatePosition();
        paint();
    }

    @Override
    public void draw() {
        super.draw();

        if (dirty) {
            paint();
            dirty = false;
        }

        document.updateMouseState();
    }

    @Override
    public void paint() {
        parent.background(255);
        document.paint();
    }

    public void updatePosition() {
        document.updateCharPosition();
        document.updateSegmentPosition();

        if (logger != null) {
            logger.log(document);
        }

    }
}
