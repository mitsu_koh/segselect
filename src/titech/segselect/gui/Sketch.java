package titech.segselect.gui;

import javax.swing.event.EventListenerList;

import titech.segselect.SegSelect;
import titech.segselect.event.SketchStoppedEvent;
import titech.segselect.event.SketchStoppedListener;

public class Sketch {
    protected SegSelect parent;
    protected EventListenerList listenerList;
    protected boolean dirty;

    public Sketch(SegSelect parent) {
        this.parent = parent;
        this.listenerList = new EventListenerList();
        dirty = true;
    }

    public void init() {
    }

    public void draw() {
    }

    public void stop() {
    }

    public void paint() {
    }

    public void addSketchStoppedListener(SketchStoppedListener l) {
        listenerList.add(SketchStoppedListener.class, l);
    }

    public void removeSketchStoppedListener(SketchStoppedListener l) {
        listenerList.remove(SketchStoppedListener.class, l);
    }

    public void fireSketchStoppedEvent(SketchStoppedEvent evt) {
        Object[] listners = listenerList.getListenerList();

        for (int i = listners.length - 2; i >= 0; i -= 2) {
            if (listners[i] == SketchStoppedListener.class) {
                ((SketchStoppedListener) listners[i + 1]).sketchStopped(evt);
            }
        }
    }

    public void setDirty(boolean dirty) {
        this.dirty = dirty;
    }
}