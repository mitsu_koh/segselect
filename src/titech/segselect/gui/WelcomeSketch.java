package titech.segselect.gui;

import java.util.ArrayList;
import java.util.List;

import titech.segselect.SegSelect;
import titech.segselect.event.SketchStoppedEvent;
import titech.segselect.experiment.Experiment;
import titech.segselect.io.ExperimentReader;
import titech.segselect.util.ControlP5Creator;
import controlP5.Button;
import controlP5.ControlEvent;
import controlP5.ControlListener;
import controlP5.ControlP5;
import controlP5.ListBox;

public class WelcomeSketch extends Sketch {
    private final static String START_BUTTON_NAME = "実験スタート";
    private final static String FAMILY_NAME_LIST_BOX_NAME = "姓";
    private final static String FIRST_NAME_LIST_BOX_NAME = "名";
    private final static String EXPERIMENT_LIST_BOX_NAME = "実験ファイル";

    private List<String> experimentList;

    private ControlP5 cp5;

    private Button startButton;
    private ListBox familyNameListBox;
    private ListBox firstNameListBox;
    private ListBox experimentListBox;

    private String selectedFamilyName;
    private String selectedFirstName;
    private Experiment selectedExperiment;

    public WelcomeSketch(SegSelect parent, List<String> experimentList) {
        super(parent);

        this.experimentList = experimentList;
    }

    @Override
    public void init() {
        super.init();

        cp5 = parent.getCP5();

        this.createGUI();
    }

    @Override
    public void draw() {
        super.draw();

        this.paint();
    }

    @Override
    public void stop() {
        super.stop();

        this.destroyGUI();
    }

    @Override
    public void paint() {
        super.paint();

        parent.background(255);
    }

    private void createGUI() {
        final List<String> alphabetList = new ArrayList<String>();
        for (int i = 0; i < 'z' - 'a' + 1; i++) {
            alphabetList.add(String.valueOf((char) ('a' + i)));
        }

        startButton = ControlP5Creator.createButton(cp5,
                START_BUTTON_NAME,
                700, 300, 200, 20);
        familyNameListBox = ControlP5Creator.createListBox(cp5,
                FAMILY_NAME_LIST_BOX_NAME,
                300, 200, 30, 20 * (alphabetList.size() + 2), 20, alphabetList);
        firstNameListBox = ControlP5Creator.createListBox(cp5,
                FIRST_NAME_LIST_BOX_NAME,
                350, 200, 30, 20 * (alphabetList.size() + 2), 20, alphabetList);
        experimentListBox = ControlP5Creator.createListBox(cp5,
                EXPERIMENT_LIST_BOX_NAME,
                400, 200, 200, 20 * (experimentList.size() + 2), 20,
                experimentList);

        startButton.addListener(new ControlListener() {
            @Override
            public void controlEvent(ControlEvent ce) {
                if (ce.getName().equals(startButton.getName())) {
                    if (selectedFamilyName != null && selectedFirstName != null
                            && selectedExperiment != null) {
                        System.out.println("Push " + startButton.getName());

                        parent.setUserName(selectedFamilyName + selectedFirstName);
                        parent.setExperiment(selectedExperiment);

                        parent.getSketch().fireSketchStoppedEvent(
                                new SketchStoppedEvent(startButton));
                    }
                }
            }
        });

        familyNameListBox.addListener(new ControlListener() {
            @Override
            public void controlEvent(ControlEvent ce) {
                if (ce.isGroup()
                        && ce.getName().equals(familyNameListBox.getName())) {
                    int selectedItem = (int) ce.getGroup().getValue();
                    selectedFamilyName = alphabetList.get(selectedItem);
                    System.out.println("FamilyName: " + selectedFamilyName);
                }
            }
        });

        firstNameListBox.addListener(new ControlListener() {
            @Override
            public void controlEvent(ControlEvent ce) {
                if (ce.isGroup()
                        && ce.getName().equals(firstNameListBox.getName())) {
                    int selectedItem = (int) ce.getGroup().getValue();
                    selectedFirstName = alphabetList.get(selectedItem);
                    System.out.println("FirstName: " + selectedFirstName);
                }
            }
        });

        experimentListBox.addListener(new ControlListener() {
            @Override
            public void controlEvent(ControlEvent ce) {
                if (ce.isGroup()
                        && ce.getName().equals(experimentListBox.getName())) {
                    int selectedItem = (int) ce.getGroup().getValue();
                    String experimentName = experimentList.get(selectedItem);
                    selectedExperiment =
                            new ExperimentReader(experimentName).read();
                    System.out.println("Experiment: " + experimentName);
                }
            }
        });
    }

    private void destroyGUI() {
        startButton.remove();
        familyNameListBox.remove();
        firstNameListBox.remove();
        experimentListBox.remove();
    }
}
