package titech.segselect.log;

import java.util.ArrayList;

import titech.segselect.display.CenteredTextDisplay;
import titech.segselect.display.CharDisplay;
import titech.segselect.display.DocumentDisplay;
import titech.segselect.display.SegmentDisplay;
import titech.segselect.event.SketchStoppedEvent;
import titech.segselect.experiment.ExperimentInfo;
import titech.segselect.io.LogPath;
import titech.segselect.io.LogWriter;

public class AnnotationLogger {
    private static String WRONG_OPERATION_LOGS_TITLE = "# WRONG_OPERATION_LOGS";
    private static String VALID_GAZE_RATIO_LOGS_TITLE = "# VALID_GAZE_RATIO_LOGS";

    private static int SMALL_CAPACITY = 1000;
    private static int LARGE_CAPACITY = 60 * 300;

    private ArrayList<Loggable> segLogs;
    private ArrayList<Loggable> charLogs;
    private ArrayList<Gaze> gazeLogs;
    private ArrayList<Loggable> mouseLogs;
    private ArrayList<Loggable> actionLogs;

    private static ArrayList<Loggable> wrongOperationLogs;
    private static ArrayList<Loggable> validGazeRatioLogs;

    private ExperimentInfo expInfo;
    private LogPath logPath;

    public static void init() {
        wrongOperationLogs = new ArrayList<Loggable>();
        validGazeRatioLogs = new ArrayList<Loggable>();

        wrongOperationLogs.ensureCapacity(SMALL_CAPACITY);
        validGazeRatioLogs.ensureCapacity(SMALL_CAPACITY);
    }

    public AnnotationLogger(ExperimentInfo expInfo) {
        this.expInfo = expInfo;
        this.logPath = new LogPath(expInfo);

        this.segLogs = new ArrayList<Loggable>();
        this.charLogs = new ArrayList<Loggable>();
        this.gazeLogs = new ArrayList<Gaze>();
        this.mouseLogs = new ArrayList<Loggable>();
        this.actionLogs = new ArrayList<Loggable>();

        this.segLogs.ensureCapacity(SMALL_CAPACITY);
        this.charLogs.ensureCapacity(SMALL_CAPACITY);
        this.gazeLogs.ensureCapacity(LARGE_CAPACITY);
        this.mouseLogs.ensureCapacity(LARGE_CAPACITY);
        this.actionLogs.ensureCapacity(LARGE_CAPACITY);
    }

    public void write(SketchStoppedEvent e) {
        Object source = e.getSource();

        if (source instanceof SegmentDisplay) {
            log(new Action(e.getTime(), Action.ANNOTATION_END,
                    ((SegmentDisplay) source).getSegment()));
            logValidGazeRatio();
        } else if (source instanceof CenteredTextDisplay) {
            log(new Action(e.getTime(), Action.ANNOTATION_END, null));
        } else {
            throw new IllegalStateException();
        }

        Object message = e.getMessage();

        if (message == null) {
            // Do nothing
        } else if (message instanceof String) {
            wrongOperationLogs.add(new Message((String) message));
        } else {
            throw new IllegalStateException();
        }

        if (segLogs.size() > 0) {
            new LogWriter(logPath.getSegLogPath(), segLogs).write();
        }
        if (charLogs.size() > 0) {
            new LogWriter(logPath.getCharLogPath(), charLogs).write();
        }
        if (gazeLogs.size() > 0) {
            new LogWriter(logPath.getGazeLogPath(), gazeLogs).write();
        }
        if (mouseLogs.size() > 0) {
            new LogWriter(logPath.getMouseLogPath(), mouseLogs).write();
        }
        if (actionLogs.size() > 0) {
            new LogWriter(logPath.getActionLogPath(), actionLogs).write();
        }
    }

    public static void writeMessageLogs(LogPath logPath) {
        ArrayList<Loggable> messageLogs = new ArrayList<Loggable>();

        messageLogs.add(new Message(WRONG_OPERATION_LOGS_TITLE));
        messageLogs.addAll(wrongOperationLogs);
        messageLogs.add(new Message(VALID_GAZE_RATIO_LOGS_TITLE));
        messageLogs.addAll(validGazeRatioLogs);

        if (messageLogs.size() > 0) {
            new LogWriter(logPath.getMessageLogPath(), messageLogs).write();
        }
    }

    public void log(CenteredTextDisplay text) {
        segLogs.add(text);
    }

    public void log(DocumentDisplay doc) {
        for (SegmentDisplay seg : doc.getSegments()) {
            segLogs.add(seg);
        }
        for (CharDisplay c : doc.getChars()) {
            charLogs.add(c);
        }
    }

    public void log(Gaze gaze) {
        gazeLogs.add(gaze);
    }

    public void log(Mouse mouse) {
        mouseLogs.add(mouse);
    }

    public void log(Action action) {
        actionLogs.add(action);
    }

    public static void logWrongOperation(String s) {
        wrongOperationLogs.add(new Message(s));
    }

    public static void logValidGazeRatio(String s) {
        validGazeRatioLogs.add(new Message(s));
    }

    private void logValidGazeRatio() {
        String documentName = expInfo.getDocument().getName();
        String validGazeRatio = calcValidGazeRatio();
        logValidGazeRatio(documentName + "\t" + validGazeRatio);
    }

    private String calcValidGazeRatio() {
        int total = gazeLogs.size();
        int valid = 0;
        for (Gaze gaze : gazeLogs) {
            if (gaze.isValid()) {
                valid++;
            }
        }

        double validRatio = (total > 0 ? 1.0D * valid / total : 0);

        return String.format("%.2f\t(%d/%d)", validRatio, valid, total);
    }
}
