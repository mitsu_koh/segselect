package titech.segselect.log;

import titech.segselect.experiment.AppConfig;

public class Gaze implements Loggable {
    private static int MAX_X = AppConfig.Sketch.SCREEN_SIZE.width;
    private static int MAX_Y = AppConfig.Sketch.SCREEN_SIZE.height;

    private int time;

    private int centerEyeX;
    private int centerEyeY;

    private int leftEyeX;
    private int leftEyeY;
    private float leftEyeDist;
    private float leftPupil;

    private int rightEyeX;
    private int rightEyeY;
    private float rightEyeDist;
    private float rightPupil;

    private long devTime;
    private int isValid;

    public Gaze() {
        time = -1;

        centerEyeX = -1;
        centerEyeY = -1;

        leftEyeX = -1;
        leftEyeY = -1;
        leftEyeDist = -1;
        leftPupil = -1;

        rightEyeX = -1;
        rightEyeY = -1;
        rightEyeDist = -1;
        rightPupil = -1;

        devTime = -1;
        isValid = 0;
    }

    @Override
    public String toString() {
        return String.format(
                "%d\t%d,%d\t%d,%d,%.3f,%.3f\t%d,%d,%.3f,%.3f\t%d,%d",
                time, centerEyeX, centerEyeY, leftEyeX, leftEyeY, leftEyeDist,
                leftPupil, rightEyeX, rightEyeY, rightEyeDist, rightPupil,
                devTime, isValid);
    }

    public boolean isValid() {
        return ((0 <= centerEyeX && centerEyeX < MAX_X) & (0 <= centerEyeY && centerEyeY < MAX_Y));
    }

    public int getCenterEyeX() {
        return centerEyeX;
    }

    public int getCenterEyeY() {
        return centerEyeY;
    }

    public int getLeftEyeX() {
        return leftEyeX;
    }

    public int getLeftEyeY() {
        return leftEyeY;
    }

    public int getRightEyeX() {
        return rightEyeX;
    }

    public int getRightEyeY() {
        return rightEyeY;
    }

    public void setTime(int time) {
        this.time = time;
    }

    public void setCenterEyeX(int centerEyeX) {
        this.centerEyeX = centerEyeX;
    }

    public void setCenterEyeY(int centerEyeY) {
        this.centerEyeY = centerEyeY;
    }

    public void setLeftEyeX(int leftEyeX) {
        this.leftEyeX = leftEyeX;
    }

    public void setLeftEyeY(int leftEyeY) {
        this.leftEyeY = leftEyeY;
    }

    public void setLeftEyeDist(float leftEyeDist) {
        this.leftEyeDist = leftEyeDist;
    }

    public void setLeftPupil(float leftPupil) {
        this.leftPupil = leftPupil;
    }

    public void setRightEyeX(int rightEyeX) {
        this.rightEyeX = rightEyeX;
    }

    public void setRightEyeY(int rightEyeY) {
        this.rightEyeY = rightEyeY;
    }

    public void setRightEyeDist(float rightEyeDist) {
        this.rightEyeDist = rightEyeDist;
    }

    public void setRightPupil(float rightPupil) {
        this.rightPupil = rightPupil;
    }

    public void setDevTime(long devTime) {
        this.devTime = devTime;
    }

    public void setIsValid(int isValid) {
        this.isValid = isValid;
    }
}