package titech.segselect.log;

import titech.segselect.experiment.AppConfig;

public class Mouse implements Loggable {
    private long time;
    private int x;
    private int y;
    private boolean isPressed;

    public Mouse(long time, int x, int y, boolean isPressed) {
        this.time = time;
        this.x = x;
        this.y = y;
        this.isPressed = isPressed;
    }

    @Override
    public String toString() {
        String ret = time + "\t" + x + "," + y + "\t";
        if (isPressed)
            ret += AppConfig.Log.PRESSED;
        return ret;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }
}
