package titech.segselect.log;

public interface Loggable {
    @Override
    public String toString();
}
