package titech.segselect.log;

import titech.segselect.annotation.Segment;

public class Action implements Loggable {
    public static String ANNOTATION_START = "annotation_start";
    public static String ANNOTATION_END = "annotation_end";
    public static String SELECT_SEGMENT = "select_segment";

    private int time;
    private String name;
    private Segment segment;

    public Action(int time, String name, Segment segment) {
        this.time = time;
        this.name = name;
        this.segment = (segment != null ? segment : new Segment());
    }

    @Override
    public String toString() {
        return time + "\t" + name + "\t" + segment.toString();
    }
}
