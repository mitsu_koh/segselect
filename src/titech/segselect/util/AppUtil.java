package titech.segselect.util;

import java.io.PrintWriter;
import java.io.StringWriter;

public class AppUtil {
    public class Pair<Key, Value> {
        public Key x;
        public Value y;

        public Pair(Key x, Value y) {
            this.x = x;
            this.y = y;
        }
    }

    public static String removeFileExtension(String filename) {
        int lastDotPos = filename.lastIndexOf('.');

        if (lastDotPos == -1) {
            return filename;
        } else if (lastDotPos == 0) {
            return filename;
        } else {
            return filename.substring(0, lastDotPos);
        }
    }

    public static final String getStackTrace(Throwable throwable) {
        String value = null;
        try {
            StringWriter sw = new StringWriter();
            try {
                throwable.printStackTrace(new PrintWriter(sw));
                value = sw.toString();
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                sw.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
            value = throwable.getMessage();
        }
        return value;
    }
}