package titech.segselect.util;

import java.util.List;

import processing.core.PApplet;
import controlP5.Button;
import controlP5.CheckBox;
import controlP5.ControlFont;
import controlP5.ControlP5;
import controlP5.ListBox;
import controlP5.Textlabel;

public class ControlP5Creator {
    public static int DARK_BLUE = 0xff02344d;

    public static ControlP5 createCP5(
            PApplet parent, String fontName, int fontSize) {

        ControlP5 cp5 = new ControlP5(parent);

        cp5.setFont(new ControlFont(parent.createFont(fontName, fontSize, true)));

        return cp5;
    }

    public static Textlabel createTextLabel(ControlP5 cp5, String name,
            int x, int y, String text) {

        Textlabel textlabel = cp5.addTextlabel(name).
                setPosition(x, y).
                setText(text);

        return textlabel;
    }

    public static Button createButton(ControlP5 cp5, String name,
            int x, int y, int width, int height) {

        Button button = cp5.addButton(name).
                setPosition(x, y).
                setSize(width, height);

        return button;
    }

    public static ListBox createListBox(ControlP5 cp5, String name,
            int x, int y, int width, int height, int itemHeight,
            List<String> keyList) {

        ListBox listBox = cp5.addListBox(name).
                setPosition(x, y).
                setSize(width, height).
                setItemHeight(itemHeight).
                setBarHeight(itemHeight);

        listBox.actAsPulldownMenu(true);
        listBox.addItems(keyList);

        return listBox;
    }

    public static CheckBox createSingleCheckBox(ControlP5 cp5, String name,
            int x, int y, int width, int height) {

        CheckBox checkBox = cp5.addCheckBox("CheckBox (" + name + ")").
                setPosition(x, y).
                setSize(width, height).
                setColorLabel(DARK_BLUE).
                addItem(name, 0);

        return checkBox;
    }
}
