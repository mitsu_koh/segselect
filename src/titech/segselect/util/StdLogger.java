package titech.segselect.util;

import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.logging.LogManager;
import java.util.logging.Logger;

import titech.segselect.experiment.AppConfig;

public class StdLogger {
    private static final Logger STD_LOGGER =
            Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

    private static final String DEFAULT_PROPERTIES =
            "handlers=java.util.logging.ConsoleHandler, java.util.logging.FileHandler\n"
            + "java.util.logging.ConsoleHandler.level=INFO\n"
            + "java.util.logging.ConsoleHandler.formatter=java.util.logging.SimpleFormatter\n"
            + "java.util.logging.FileHandler.level=INFO\n"
            + "java.util.logging.FileHandler.pattern=segselect.log\n"
            + "java.util.logging.FileHandler.formatter=java.util.logging.SimpleFormatter\n"
            + "java.util.logging.FileHandler.append=false\n"
            + "java.util.logging.FileHandler.encoding=utf-8\n";

    public static void init() {
        try {
            STD_LOGGER.info("Trying to load "
                    + AppConfig.General.STD_LOG_PROPERTY + ".");
            InputStream is = new FileInputStream(
                    AppConfig.General.STD_LOG_PROPERTY);

            try {
                LogManager.getLogManager().readConfiguration(is);
                STD_LOGGER.info("LogManager was configured from the file.");
            } catch (Exception e) {
                STD_LOGGER.warning(e.toString());
            } finally {
                is.close();
            }
        } catch (IOException e) {
            STD_LOGGER.warning(e.toString());
            setDefault();
        }
    }

    public static void setDefault() {
        try {
            InputStream is = new ByteArrayInputStream(DEFAULT_PROPERTIES.getBytes());

            try {
                LogManager.getLogManager().readConfiguration(is);
                STD_LOGGER.info("LogManager was configured as default.");
            } catch (Exception e) {
                STD_LOGGER.warning(e.toString());
            } finally {
                is.close();
            }
        } catch (IOException e) {
            STD_LOGGER.warning(e.toString());
        }
    }

    public static Logger getInstance() {
        return STD_LOGGER;
    }
}
