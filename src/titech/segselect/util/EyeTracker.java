package titech.segselect.util;

import java.awt.Point;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.logging.Logger;

import titech.segselect.SegSelect;
import titech.segselect.log.Gaze;
import net.xeoh.plugins.base.PluginManager;
import net.xeoh.plugins.base.impl.PluginManagerFactory;
import net.xeoh.plugins.base.options.getplugin.OptionCapabilities;
import de.dfki.km.text20.services.trackingdevices.eyes.EyeTrackingDevice;
import de.dfki.km.text20.services.trackingdevices.eyes.EyeTrackingDeviceProvider;
import de.dfki.km.text20.services.trackingdevices.eyes.EyeTrackingEvent;
import de.dfki.km.text20.services.trackingdevices.eyes.EyeTrackingEventValidity;
import de.dfki.km.text20.services.trackingdevices.eyes.EyeTrackingListener;

public class EyeTracker {
    private static final String TOBII_ADDRESS = "discover://nearest";

    private static final Logger STANDARD_LOGGER =
            Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

    private SegSelect parent;
    private EyeTrackingDevice device;
    private EyeTrackingEvent lastEyeTrackingEvent;

    public EyeTracker(SegSelect par) {
        parent = par;
        device = null;
        lastEyeTrackingEvent = null;
    }

    public boolean init() {
        STANDARD_LOGGER.info("Trying to connect to eye tracker.");
        boolean found = initDevice();
        if (found) {
            initListener();
            STANDARD_LOGGER.info("Connected to Tobii.");
        }
        return found;
    }

    public Gaze createGaze() {
        Gaze gaze = new Gaze();
        gaze.setTime(parent.millis());

        EyeTrackingEvent last = this.getLastEyeTrackingEvent();

        // FIXME last.get{Left,Right}EyeGazePoint is
        // not implemented in text20.jar
        if (last != null) {
            Point gazeCenter = last.getGazeCenter();
            // Point leftGaze = last.getLeftEyeGazePoint();
            // Point rightGaze = last.getRightEyeGazePoint();

            gaze.setDevTime(last.getObservationTime());
            gaze.setIsValid(last.areValid(
                    EyeTrackingEventValidity.CENTER_POSITION_VALID) ? 1 : 0);

            gaze.setCenterEyeX(gazeCenter.x);
            gaze.setCenterEyeY(gazeCenter.y);

            // gaze.setLeftEyeX(leftGaze.x);
            // gaze.setLeftEyeY(leftGaze.y);
            gaze.setLeftEyeDist(last.getLeftEyeDistance());
            gaze.setLeftPupil(last.getPupilSizeLeft());

            // gaze.setRightEyeX(rightGaze.x);
            // gaze.setRightEyeY(rightGaze.y);
            gaze.setRightEyeDist(last.getRightEyeDistance());
            gaze.setRightPupil(last.getPupilSizeRight());
        }

        return gaze;
    }

    private boolean initDevice() {
        try {
            // Start the framework. Do this only *once* in your whole
            // application.
            PluginManager pluginManager = PluginManagerFactory
                    .createPluginManager();
            pluginManager.addPluginsFrom(new URI("classpath://*"));

            // Obtain an eye tracking device connection. In this case, the next
            // best tracking server will be found.
            EyeTrackingDeviceProvider deviceProvider = pluginManager.getPlugin(
                    EyeTrackingDeviceProvider.class, new OptionCapabilities(
                            "eyetrackingdevice:trackingserver"));
            device = deviceProvider.openDevice(TOBII_ADDRESS);
            if (device == null) {
                STANDARD_LOGGER.severe("Error opening device `" + TOBII_ADDRESS
                        + "'. Most likely there is no TrackingServer running!");
            }
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }

        return (device != null ? true : false);
    }

    private void initListener() {
        device.addTrackingListener(new EyeTrackingListener() {
            @Override
            public void newTrackingEvent(EyeTrackingEvent evt) {
                lastEyeTrackingEvent = evt;
            }
        });
    }

    private EyeTrackingEvent getLastEyeTrackingEvent() {
        return lastEyeTrackingEvent;
    }
}
