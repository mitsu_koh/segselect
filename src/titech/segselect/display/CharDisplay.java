package titech.segselect.display;

import processing.core.PApplet;
import titech.segselect.annotation.Char;
import titech.segselect.experiment.AppConfig;
import titech.segselect.log.Loggable;

public class CharDisplay implements Loggable {
    private static int OFFSET_X = AppConfig.Sketch.SKETCH_OFFSET.width;
    private static int OFFSET_Y = AppConfig.Sketch.SKETCH_OFFSET.height;

    private int COLOR = 0;

    private Char c;
    private int row;
    private int col;
    private int x;
    private int y;
    private int width;
    private int height;

    public CharDisplay(Char c) {
        this.c = c;
        row = -1;
        col = -1;
        x = -1;
        y = -1;
        width = AppConfig.Sketch.FONT_SIZE;
        height = AppConfig.Sketch.FONT_SIZE;
    }

    @Override
    public String toString() {
        return c.getOffset() + "\t"
                + c.getC() + "\t"
                + row + "," + col + "\t"
                + x + "," + y + "," + width + "," + height;
    }

    // No calculation version
    // Document file has line adjustment
    public void updatePosition() {
        row = c.getLineOffset();
        col = c.getParOffset();
        x = OFFSET_X + width * col + AppConfig.Sketch.CHAR_SPACE * col;
        y = OFFSET_Y + height * row + AppConfig.Sketch.LINE_SPACE * row;
    }

    // Automatic calculation version
    // Document file has no new line adjustment
    public int updatePosition(int rowBaseOffset, int charPerLine) {
        if (c.isNewLine()) {
            int prevCharOffset =
                    c.getParOffset() > 0 ? c.getParOffset() - 1 : 0;
            int rowParOffset = prevCharOffset / charPerLine;
            row = -1;
            col = -1;
            x = -1;
            y = -1;
            return rowBaseOffset + rowParOffset + 1;
        } else {
            int rowParOffset = c.getParOffset() / charPerLine;
            row = rowBaseOffset + rowParOffset;
            col = c.getParOffset() % charPerLine;
            x = OFFSET_X + width * col + AppConfig.Sketch.CHAR_SPACE * col;
            y = OFFSET_Y + height * row + AppConfig.Sketch.LINE_SPACE * row;
            return rowBaseOffset;
        }
    }

    public void draw(PApplet parent) {
        if (c.isNewLine()) {
            return;
        } else {
            parent.fill(COLOR);
            parent.text(c.getC(), x, y + AppConfig.Sketch.FONT_SIZE - 1);
        }
    }

    public Char getChar() {
        return c;
    }

    public int getRow() {
        return row;
    }

    public void setRow(int row) {
        this.row = row;
    }

    public int getCol() {
        return col;
    }

    public void setCol(int col) {
        this.col = col;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }
}
