package titech.segselect.display;

import java.util.ArrayList;

import processing.core.PApplet;
import titech.segselect.annotation.Segment;
import titech.segselect.experiment.AppConfig;
import titech.segselect.log.Loggable;

public class InnerSegmentDisplay implements Loggable {
    public enum Place {
        WHOLE, HEAD, MID, TAIL
    };

    private SegmentDisplay segment;
    private Place place;
    private ArrayList<CharDisplay> chars;
    private int x;
    private int y;
    private int width;
    private int height;
    private int tl;
    private int tr;
    private int br;
    private int bl;

    public InnerSegmentDisplay(SegmentDisplay segment,
            ArrayList<CharDisplay> chars, Place place) {
        this.segment = segment;
        this.chars = chars;
        this.place = place;

        CharDisplay a = chars.get(0);
        CharDisplay b = chars.get(chars.size() - 1);
        x = a.getX();
        y = a.getY() - AppConfig.Sketch.SEGMENT_MARGIN_TOP;
        width = b.getX() + b.getWidth() - a.getX();
        height = a.getHeight() + AppConfig.Sketch.SEGMENT_MARGIN_TOP * 2;

        int r = AppConfig.Sketch.SEGMENT_RADIUS;
        if (this.place == Place.WHOLE) {
            tl = tr = br = bl = r;
        } else {
            tl = (this.place == Place.HEAD ? r : 0);
            tr = (this.place == Place.TAIL ? r : 0);
            br = tr;
            bl = tl;
        }
    }

    @Override
    public String toString() {
        return chars.size() + ","
                + x + "," + y + "," + width + "," + height;
    }

    public boolean isMouseOver(int mouseX, int mouseY) {
        return (mouseX >= x && mouseX <= x + width &&
                mouseY >= y && mouseY <= y + height);
    }

    public void draw(PApplet parent) {
        int bgColor;
        int borderColor;
        int strokeWeight;

        if (segment.getSegment().getName().equals(Segment.NOUN)) {
            bgColor = AppConfig.Sketch.NOUN_BG_COLOR.getRGB();
            borderColor = AppConfig.Sketch.NOUN_BORDER_COLOR.getRGB();
        } else {
            bgColor = AppConfig.Sketch.PRED_BG_COLOR.getRGB();
            borderColor = AppConfig.Sketch.PRED_BORDER_COLOR.getRGB();
        }

        if (segment.getClickCount() == 0) {
            strokeWeight = AppConfig.Sketch.SEGMENT_BOARDER_THICK;
        } else {
            strokeWeight = AppConfig.Sketch.CLICKED_SEGMENT_BOARDER_THICK;
        }

        parent.fill(bgColor);
        parent.stroke(borderColor);
        parent.strokeWeight(strokeWeight);
        parent.rect(x, y, width, height, tl, tr, br, bl);
    }
}