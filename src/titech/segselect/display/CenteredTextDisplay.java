package titech.segselect.display;

import java.util.logging.Logger;

import titech.segselect.SegSelect;
import titech.segselect.event.SketchStoppedEvent;
import titech.segselect.experiment.AppConfig;
import titech.segselect.log.Loggable;

public class CenteredTextDisplay implements Loggable {
    private static final int RECT_TOP_MARGIN = 10;

    private static final Logger STANDARD_LOGGER =
            Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

    private SegSelect parent;
    private int x;
    private int y;
    private int width;
    private int height;
    private int fontSize;
    private String text;

    public CenteredTextDisplay(SegSelect parent, String text, int x, int y) {
        this.parent = parent;
        this.text = text;
        this.x = x;
        this.y = y;
        this.width = (int) (parent.textWidth(text));
        this.height = AppConfig.Sketch.FONT_SIZE + RECT_TOP_MARGIN;
        this.fontSize = AppConfig.Sketch.FONT_SIZE;
    }

    @Override
    public String toString() {
        int tlx = x - width / 2;
        int tly = y - height / 2;
        return tlx + "," + tly + "," + width + "," + height + "\t" + text;
    }

    public void draw() {
        float rectTlx = x - width / 2;
        float rectTly = y - height / 2;
        float textBlx = x - width / 2;
        float textBly = y + fontSize / 2;
        parent.noFill();
        parent.stroke(0);
        parent.rect(rectTlx, rectTly, width, height);
        parent.strokeWeight(AppConfig.Sketch.TEXTBOX_BORDER_THICK);
        parent.fill(0);
        parent.text(text, textBlx, textBly);
    }

    public void updateMouseState() {
        if (parent.isClicked() && isMouseOver()) {
            STANDARD_LOGGER.info("Text box " + text + "is clicked.");
            parent.getSketch().fireSketchStoppedEvent(
                    new SketchStoppedEvent(this, parent.millis()));
        }
    }

    private boolean isMouseOver() {
        int tlx = x - width / 2;
        int tly = y - height / 2;
        int px = parent.mouseX;
        int py = parent.mouseY;
        return (px >= tlx && px <= tlx + width &&
                py >= tly && py <= tly + height);
    }
}
