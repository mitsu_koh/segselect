package titech.segselect.display;

import java.util.ArrayList;
import java.util.logging.Logger;

import titech.segselect.SegSelect;
import titech.segselect.annotation.Char;
import titech.segselect.annotation.Document;
import titech.segselect.annotation.Segment;
import titech.segselect.event.SketchStoppedEvent;
import titech.segselect.log.Action;
import titech.segselect.log.AnnotationLogger;
import titech.segselect.log.Loggable;

public class DocumentDisplay implements Loggable {
    private static final Logger STD_LOGGER =
            Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

    private SegSelect parent;
    private Document document;
    private ArrayList<CharDisplay> chars;
    private ArrayList<SegmentDisplay> segments;

    public DocumentDisplay(SegSelect parent, Document document) {
        this.parent = parent;
        this.document = document;

        chars = new ArrayList<CharDisplay>();
        segments = new ArrayList<SegmentDisplay>();

        for (Char c : document.getChars())
            chars.add(new CharDisplay(c));

        for (Segment seg : document.getSegments()) {
            int start = seg.getStart() >= 0 ? seg.getStart() : 0;
            int end = seg.getEnd();
            if (start < end) {
                ArrayList<CharDisplay> segChars = new ArrayList<CharDisplay>(
                        chars.subList(start, end));
                segments.add(new SegmentDisplay(seg, segChars));
            }
        }
    }

    // See CharDisplay
    // public void updateCharPosition() {
    //     int margin = SegSelectConfig.Sketch.SCREEN_SIZE.width - SegSelectConfig.Sketch.SKETCH_SIZE.width;
    //     int width = parent.width - margin;
    //     int charPerLine = (width + SegSelectConfig.Sketch.CHAR_SPACE)
    //             / (SegSelectConfig.Sketch.FONT_SIZE + SegSelectConfig.Sketch.CHAR_SPACE);
    //     int rowBaseOffset = 0;

    //     for (CharDisplay c : chars) {
    //         rowBaseOffset = c.updatePosition(rowBaseOffset, charPerLine);
    //     }
    // }

    // See CharDisplay
    public void updateCharPosition() {
        for (CharDisplay c : chars) {
            c.updatePosition();
        }
    }

    public void updateSegmentPosition() {
        for (SegmentDisplay segDisp : segments) {
            segDisp.updatePostion();
        }
    }

    public void updateMouseState() {
        if (parent.isClicked()) {
            for (SegmentDisplay seg : segments) {
                if (seg.isMouseOver(parent.mouseX, parent.mouseY)) {
                    int time = parent.millis();
                    AnnotationLogger annotationLogger = parent.getAnnotationLogger();

                    seg.click();
                    parent.getSketch().setDirty(true);

                    if (annotationLogger != null) {
                        annotationLogger.log(
                            new Action(time, Action.SELECT_SEGMENT, seg.getSegment()));
                    }
                    STD_LOGGER.info("Segment "
                            + seg.getSegment().toString() + " is clicked.");

                    if (seg.isSelected()) {
                        parent.getSketch().fireSketchStoppedEvent(
                                new SketchStoppedEvent(seg, time));
                    }
                }
            }
        }
    }

    public void paint() {
        for (SegmentDisplay seg : segments) {
            seg.draw(parent);
        }
        for (CharDisplay c : chars) {
            c.draw(parent);
        }
    }

    public Document getDocument() {
        return document;
    }

    public ArrayList<SegmentDisplay> getSegments() {
        return segments;
    }

    public ArrayList<CharDisplay> getChars() {
        return chars;
    }
}