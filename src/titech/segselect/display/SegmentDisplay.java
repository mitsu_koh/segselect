package titech.segselect.display;

import java.util.ArrayList;

import processing.core.PApplet;
import titech.segselect.annotation.Segment;
import titech.segselect.experiment.AppConfig;
import titech.segselect.log.Loggable;

public class SegmentDisplay implements Loggable {
    private Segment segment;
    private ArrayList<CharDisplay> chars;
    private ArrayList<InnerSegmentDisplay> parts;
    private int clickCount;

    public SegmentDisplay(Segment segment, ArrayList<CharDisplay> chars) {
        this.segment = segment;
        this.chars = chars;
        parts = new ArrayList<InnerSegmentDisplay>();
        clickCount = 0;
    }

    @Override
    public String toString() {
        String seg = segment.toString();
        String loc = chars.get(0).getRow() + "," + chars.get(0).getCol();
        String prt = "";
        for (InnerSegmentDisplay p : parts) {
            prt += prt.isEmpty() ? "" : "_";
            prt += p.toString();
        }
        return seg + "\t" + loc + "\t" + prt;
    }

    public void updatePostion() {
        parts.clear();

        CharDisplay from = chars.get(0);
        int startOffset = from.getChar().getOffset();
        InnerSegmentDisplay.Place place = InnerSegmentDisplay.Place.HEAD;
        boolean wrapped = false;

        for (CharDisplay upto : chars) {
            if (upto.getRow() != from.getRow()) {
                int fromOffset = from.getChar().getOffset() - startOffset;
                int uptoOffset = upto.getChar().getOffset() - startOffset;
                ArrayList<CharDisplay> partChars = new ArrayList<CharDisplay>(
                        chars.subList(fromOffset, uptoOffset));
                parts.add(new InnerSegmentDisplay(this, partChars, place));
                place = InnerSegmentDisplay.Place.MID;

                from = upto;
                wrapped = true;
            }
        }
        int fromOffset = from.getChar().getOffset() - startOffset;
        int uptoOffset = chars.size();
        if (fromOffset != uptoOffset) {
            place = (wrapped ? InnerSegmentDisplay.Place.TAIL
                    : InnerSegmentDisplay.Place.WHOLE);
            ArrayList<CharDisplay> partChars = new ArrayList<CharDisplay>(
                    chars.subList(fromOffset, uptoOffset));
            parts.add(new InnerSegmentDisplay(this, partChars, place));
        }
    }

    public boolean isMouseOver(int mouseX, int mouseY) {
        boolean over = false;
        for (InnerSegmentDisplay p : parts) {
            over |= p.isMouseOver(mouseX, mouseY);
        }
        return over;
    }

    public void draw(PApplet parent) {
        for (InnerSegmentDisplay p : parts) {
            p.draw(parent);
        }
    }

    public void click() {
        clickCount += 1;
    }

    public boolean isClicked() {
        return clickCount != 0;
    }

    public boolean isSelected() {
        return clickCount >= AppConfig.General.SELECT_CLICK_COUNT;
    }

    public Segment getSegment() {
        return segment;
    }

    public ArrayList<InnerSegmentDisplay> getParts() {
        return parts;
    }

    public int getClickCount() {
        return clickCount;
    }
}
