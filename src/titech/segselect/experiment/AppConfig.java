package titech.segselect.experiment;

import java.awt.Color;
import java.awt.Dimension;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import titech.segselect.util.StdLogger;

public class AppConfig {
    public static void init() {
        try {
            StdLogger.getInstance().info("Trying to load "
                    + AppConfig.General.SEG_SELECT_PROPERTY + ".");
            InputStream is = new FileInputStream(
                    AppConfig.General.SEG_SELECT_PROPERTY);

             try {
                Properties prop = new Properties();
                prop.load(is);
                AppConfig.General.USE_TOBII =
                        Boolean.parseBoolean(prop.getProperty("USE_TOBII"));
                AppConfig.General.SELECT_CLICK_COUNT =
                        Integer.parseInt(prop.getProperty("SELECT_CLICK_COUNT"));
                StdLogger.getInstance().info("AppConfig was configured from the file.");
            } catch (Exception e) {
                StdLogger.getInstance().warning(e.toString());
            } finally {
                is.close();
            }
        } catch (IOException e) {
            StdLogger.getInstance().warning(e.toString());
        }
    }

    public static class General {
        public static final String ENCODING = "UTF-8";
        public static final int INTERMEDIATE_SKETCH_ID = 0;
        public static final int ANNOTATION_SKETCH_ID = 1;
        public static final String STD_LOG_PROPERTY = "./javalog.properties";
        public static final String SEG_SELECT_PROPERTY = "./segselect.properties";
        public static boolean USE_TOBII = true;
        public static int SELECT_CLICK_COUNT = 2;
    }

    public static class Sketch {
        public static final int FONT_SIZE = 16;
        public static final int CHAR_SPACE = 1;
        public static final int LINE_SPACE = 66;
        public static final String FONT_NAME = "ＭＳゴシック";

        public static final int SEGMENT_MARGIN_TOP = 2;
        public static final int SEGMENT_RADIUS = 5;
        public static final int SEGMENT_BOARDER_THICK = 1;
        public static final int TEXTBOX_BORDER_THICK = 1;
        public static final int CLICKED_SEGMENT_BOARDER_THICK = 5;
        public static final Color PRED_BG_COLOR = Color.cyan;
        public static final Color NOUN_BG_COLOR = Color.lightGray;
        public static final Color PRED_BORDER_COLOR = Color.blue;
        public static final Color NOUN_BORDER_COLOR = Color.pink;

        public static final Dimension SCREEN_SIZE = new Dimension(1280, 1024);
        public static final Dimension SKETCH_SIZE = new Dimension(1258, 1000);
        public static final Dimension SKETCH_OFFSET = new Dimension(11, 12);
    }

    public static class Document {
        public static final String PATH = "./exp";
        public static final String DOC_EXT = "txt";
        public static final String EXP_EXT = "exp";
        public static final String NEW_LINE = "\n";
        public static final String DELIMITER = "#";
        public static final String PRED = "pred";
        public static final String NOUN = "noun";
    }

    public static class Log {
        public static final String LOG = "./log";
        public static final String PRESSED = "pressed";
        public static final String LOG_EXT = "log";
        public static final String SEG_EXT = "seg";
        public static final String CHAR_EXT = "chr";
        public static final String GAZE_EXT = "eye";
        public static final String MOUSE_EXT = "cur";
        public static final String ACTION_EXT = "act";
        public static final String MESSAGE_EXT = "msg";
    }
}
