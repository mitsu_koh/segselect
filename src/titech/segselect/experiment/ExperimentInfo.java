package titech.segselect.experiment;

import titech.segselect.annotation.Document;

public class ExperimentInfo {
    private String annotator;
    private Experiment experiment;
    private Document document;
    private int type;

    public ExperimentInfo(String annotator, Experiment experiment,
            Document document, int type) {
        this.annotator = annotator;
        this.experiment = experiment;
        this.document = document;
        this.type = type;
    }

    public String getAnnotator() {
        return annotator;
    }

    public Document getDocument() {
        return document;
    }

    public Experiment getExperiment() {
        return experiment;
    }

    public int getType() {
        return type;
    }
}
