package titech.segselect.experiment;

import java.util.ArrayList;

import titech.segselect.annotation.Document;

public class Experiment {
    private String path;
    private String name;
    private ArrayList<Document> documents;

    public Experiment(String path, String name, ArrayList<Document> documents) {
        this.path = path;
        this.name = name;
        this.documents = documents;
    }

    public ArrayList<Document> getDocuments() {
        return documents;
    }

    public Document getDocument(int i) {
        return documents.get(i);
    }

    public String getPath() {
        return path;
    }

    public int getSize() {
        return documents.size();
    }

    public String getName() {
        return name;
    }
}
