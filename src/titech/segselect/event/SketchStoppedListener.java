package titech.segselect.event;

import java.util.EventListener;

public interface SketchStoppedListener extends EventListener {
    public void sketchStopped(SketchStoppedEvent e);
}
