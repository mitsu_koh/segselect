package titech.segselect.event;

import java.util.EventObject;

public class SketchStoppedEvent extends EventObject {
    private static final long serialVersionUID = 1L;

    private int time;
    private Object message;

    public SketchStoppedEvent(Object source) {
        this(source, -1, null);
    }

    public SketchStoppedEvent(Object source, int time) {
        this(source, time, null);
    }

    public SketchStoppedEvent(Object source, Object message) {
        this(source, -1, message);
    }

    public SketchStoppedEvent(Object source, int time, Object message) {
        super(source);
        this.time = time;
        this.message = message;
    }

    public int getTime() {
        return time;
    }

    public Object getMessage() {
        return message;
    }
}
