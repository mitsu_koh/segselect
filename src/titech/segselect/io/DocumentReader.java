package titech.segselect.io;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

import titech.segselect.annotation.Document;
import titech.segselect.experiment.AppConfig;
import titech.segselect.util.AppUtil;

public class DocumentReader {
    private String path;

    public DocumentReader(String path) {
        this.path = path;
    }

    public DocumentReader(File file) {
        this(file.getAbsolutePath());
    }

    public Document read() {
        ArrayList<String> lines = new ArrayList<String>();
        String name = AppUtil.removeFileExtension(new File(path).getName());
        String line;

        try {
            BufferedReader br = new BufferedReader(
                    new InputStreamReader(
                            new FileInputStream(path),
                            AppConfig.General.ENCODING));
            try {
                while ((line = br.readLine()) != null) {
                    lines.add(line);
                }
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                br.close();
            }
        } catch (IOException e) {
            System.out.println(e);
        }

        return new Document(path, name, lines);
    }
}