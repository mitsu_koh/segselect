package titech.segselect.io;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;

import titech.segselect.experiment.AppConfig;
import titech.segselect.log.Loggable;

public class LogWriter {
    private String path;
    private ArrayList<? extends Loggable> logs;

    public LogWriter(String path, ArrayList<? extends Loggable> logs) {
        this.path = path;
        this.logs = logs;
    }

    public void write() {
        File file = new File(path);
        File parent = file.getParentFile();
        if (!parent.exists()) {
            parent.mkdirs();
        }

        try {
            OutputStreamWriter sw = new OutputStreamWriter(
                    new FileOutputStream(path), AppConfig.General.ENCODING);
            try {
                for (Loggable l : logs) {
                    sw.write(l.toString() + "\n");
                }
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                sw.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
