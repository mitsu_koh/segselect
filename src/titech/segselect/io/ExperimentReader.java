package titech.segselect.io;

import java.io.File;
import java.io.FilenameFilter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import titech.segselect.annotation.Document;
import titech.segselect.experiment.Experiment;
import titech.segselect.experiment.AppConfig;
import titech.segselect.util.AppUtil;

public class ExperimentReader {
    private String ext = AppConfig.Document.DOC_EXT;
    private String path = AppConfig.Document.PATH;
    private String fileName;

    public ExperimentReader(String fileName) {
        this.fileName = fileName;
    }

    public Experiment read() {
        ArrayList<Document> documents = new ArrayList<Document>();
        File[] founds = new File(getFilePath()).listFiles(new FilenameFilter() {
            @Override
            public boolean accept(File dir, String name) {
                return name.endsWith(ext);
            }
        });

        for (File file : founds) {
            documents.add(new DocumentReader(file).read());
        }
        Collections.sort(documents, new Comparator<Document>() {
            @Override
            public int compare(Document a, Document b) {
                return a.getName().compareTo(b.getName());
            }
        });

        String name = AppUtil.removeFileExtension(
                new File(getFilePath()) .getName());

        return new Experiment(path, name, documents);
    }

    private String getFilePath() {
        return path + File.separator + this.fileName;
    }

    public String getPath() {
        return path;
    }
}
