package titech.segselect.io;

import java.io.File;
import java.io.FilenameFilter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

import titech.segselect.experiment.AppConfig;

public class ExperimentListReader {
    private String path = AppConfig.Document.PATH;
    private String ext = AppConfig.Document.EXP_EXT;

    public ExperimentListReader() {
    }

    public ArrayList<String> read() {
        String[] found = new File(path).list(new FilenameFilter() {
            @Override
            public boolean accept(File dir, String name) {
                return name.endsWith(ext);
            }
        });
        ArrayList<String> fileList = new ArrayList<String>(Arrays.asList(found));
        Collections.sort(fileList);

        return fileList;
    }
}
