package titech.segselect.io;

import java.io.File;

import processing.core.PApplet;
import titech.segselect.experiment.ExperimentInfo;
import titech.segselect.experiment.AppConfig;

public class LogPath {
    private String annotator;
    private String docName;
    private String expName;
    private int type;

    static private int year;
    static private int month;
    static private int day;
    static private int hour;
    static private int minute;
    static private int second;

    public LogPath(ExperimentInfo experimentInfo) {
        this.annotator = experimentInfo.getAnnotator();
        if (experimentInfo.getDocument() != null) {
            this.docName = experimentInfo.getDocument().getName();
        }
        this.expName = experimentInfo.getExperiment().getName();
        this.type = experimentInfo.getType();
    }

    public static void initDate() {
        year   = PApplet.year();
        month  = PApplet.month();
        day    = PApplet.day();
        hour   = PApplet.hour();
        minute = PApplet.minute();
        second = PApplet.second();
    }

    public String getSegLogPath() {
        String ext = AppConfig.Log.SEG_EXT;
        return getDirPath() + File.separator + getFileName(ext);
    }

    public String getCharLogPath() {
        String ext = AppConfig.Log.CHAR_EXT;
        return getDirPath() + File.separator + getFileName(ext);
    }

    public String getGazeLogPath() {
        String ext = AppConfig.Log.GAZE_EXT;
        return getDirPath() + File.separator + getFileName(ext);
    }

    public String getMouseLogPath() {
        String ext = AppConfig.Log.MOUSE_EXT;
        return getDirPath() + File.separator + getFileName(ext);
    }

    public String getActionLogPath() {
        String ext = AppConfig.Log.ACTION_EXT;
        return getDirPath() + File.separator + getFileName(ext);
    }

    public String getMessageLogPath() {
        String ext = AppConfig.Log.MESSAGE_EXT;
        return getTopDirPath() + File.separator + getMessageLogFileName(ext);
    }

    private String getTypeDirName() {
        if (type == AppConfig.General.INTERMEDIATE_SKETCH_ID) {
            return String.valueOf(type);
        } else if (type == AppConfig.General.ANNOTATION_SKETCH_ID) {
            return String.valueOf(type);
        } else {
            throw new IllegalStateException();
        }
    }

    private String getDirPath() {
        return getTopDirPath() + File.separator
                + getTypeDirName() + File.separator
                + docName;
    }

    private String getTopDirPath() {
        return AppConfig.Log.LOG + File.separator
                + getPrettyDate() + "."
                + annotator + "."
                + expName + "."
                + AppConfig.Log.LOG_EXT;
    }

    private String getFileName(String ext) {
        return getPrettyDate() + "."
                + annotator + "."
                + expName + "."
                + getTypeDirName() + "."
                + docName + "."
                + ext;
    }

    private String getMessageLogFileName(String ext) {
        return getPrettyDate() + "."
                + annotator + "."
                + expName + "."
                + ext;
    }

    private String getPrettyDate() {
        return String.format("%d-%02d-%02d-%02d-%02d-%02d",
                year, month, day, hour, minute, second);
    }
}
