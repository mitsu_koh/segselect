package titech.segselect;

import controlP5.ControlP5;
import processing.core.PApplet;
import titech.segselect.annotation.Document;
import titech.segselect.event.SketchStoppedEvent;
import titech.segselect.event.SketchStoppedListener;
import titech.segselect.experiment.Experiment;
import titech.segselect.experiment.ExperimentInfo;
import titech.segselect.experiment.AppConfig;
import titech.segselect.gui.AnnotationSketch;
import titech.segselect.gui.IntermediateSketch;
import titech.segselect.gui.Sketch;
import titech.segselect.gui.WelcomeSketch;
import titech.segselect.io.ExperimentListReader;
import titech.segselect.io.LogPath;
import titech.segselect.log.Action;
import titech.segselect.log.AnnotationLogger;
import titech.segselect.log.Gaze;
import titech.segselect.log.Mouse;
import titech.segselect.util.ControlP5Creator;
import titech.segselect.util.EyeTracker;
import titech.segselect.util.AppUtil;
import titech.segselect.util.StdLogger;

public class SegSelect extends PApplet implements SketchStoppedListener {
    private static final long serialVersionUID = 1L;

    private static final int WIDTH = AppConfig.Sketch.SCREEN_SIZE.width;
    private static final int HEIGHT = AppConfig.Sketch.SCREEN_SIZE.height;
    private static final String FONT_NAME = AppConfig.Sketch.FONT_NAME;
    private static final int FONT_SIZE = AppConfig.Sketch.FONT_SIZE;

    private EyeTracker tracker;
    private AnnotationLogger annotationLogger;

    private ControlP5 cp5;

    private Experiment experiment;
    private String userName;
    private int documentIndex;
    private Sketch sketch;
    private boolean pmousePressed;

    public boolean debug;

    public SegSelect() {
        super();

        try {
            StdLogger.init();
            AppConfig.init();
            AnnotationLogger.init();
            LogPath.initDate();

            tracker = null;
            annotationLogger = null;

            if (AppConfig.General.USE_TOBII) {
                tracker = new EyeTracker(this);
                if (!tracker.init()) {
                    die("Can't find eye tracker and exit.");
                }
            }

            cp5 = null;

            setExperiment(null);
            setUserName(null);
            documentIndex = -1;
            sketch = null;
            pmousePressed = false;

            debug = false;
        } catch (Exception e) {
            StdLogger.getInstance().severe(AppUtil.getStackTrace(e));
            die("Exception occurred and exit.");
        }
    }

    @Override
    public void setup() {
        try {
            size(WIDTH, HEIGHT);
            textFont(createFont(FONT_NAME, FONT_SIZE, true), FONT_SIZE);

            cp5 = ControlP5Creator.createCP5(this, FONT_NAME, FONT_SIZE);

            sketchStopped(null);
        } catch (Exception e) {
            StdLogger.getInstance().severe(AppUtil.getStackTrace(e));
            die("Exception occurred and exit.");
        }
    }

    @Override
    public void draw() {
        try {
            Gaze gaze = tracker != null ? tracker.createGaze() : null;
            Mouse mouse = this.createMouse();

            if (debug) {
                fill(color(0, 0, 255));
                ellipse(gaze.getCenterEyeX(), gaze.getCenterEyeY(), 10, 10);
                fill(color(255, 0, 0));
                ellipse(mouse.getX(), mouse.getY(), 10, 10);
            }

            if (annotationLogger != null && gaze != null) {
                annotationLogger.log(gaze);
                annotationLogger.log(mouse);
            }

            sketch.draw();

            this.updateMouseState();
        } catch (Exception e) {
            StdLogger.getInstance().severe(AppUtil.getStackTrace(e));
            die("Exception occurred and exit.");
        }
    }

    @Override
    public void sketchStopped(SketchStoppedEvent sse) {
        if (sketch != null) {
            sketch.stop();
        }

        if (annotationLogger != null) {
            annotationLogger.write(sse);
        }

        sketch = createNextSketch();

        if (sketch != null) {
            sketch.init();
        }
    }

    @Override
    public void die(String what) {
        StdLogger.getInstance().severe(what);
        System.exit(-1);
    }

    public boolean isClicked() {
        return mousePressed & !pmousePressed;
    }

    private Sketch createNextSketch() {
        if (sketch == null) {

            return createWelcomeSketch();

        } else if (sketch instanceof WelcomeSketch) {

            documentIndex++;
            if (documentIndex == getExperiment().getSize()) {
                StdLogger.getInstance().info("No document exists and exit.");
                System.exit(0);
            }
            return createInterMediateSketch();

        } else if (sketch instanceof IntermediateSketch) {

            if (documentIndex == getExperiment().getSize()) {
                finishSuccessfully();
            }
            return createAnnotationSketch();

        } else if (sketch instanceof AnnotationSketch) {

            documentIndex++;
            return createInterMediateSketch();

        } else {

            throw new IllegalStateException();

        }
    }

    private void updateMouseState() {
        pmousePressed = mousePressed;
    }

    private AnnotationLogger createAnnotationLogger(int type) {
        AnnotationLogger ret = null;
        if (AppConfig.General.USE_TOBII && getDocument() != null) {
            ret = new AnnotationLogger(new ExperimentInfo(
                    getUserName(), getExperiment(), getDocument(), type));
        }
        return ret;
    }

    private Sketch createWelcomeSketch() {
        Sketch ret = new WelcomeSketch(this, new ExperimentListReader().read());

        ret.addSketchStoppedListener(this);

        return ret;
    }

    private Sketch createInterMediateSketch() {
        annotationLogger = createAnnotationLogger(
                AppConfig.General.INTERMEDIATE_SKETCH_ID);

        Sketch ret = new IntermediateSketch(this,
                documentIndex, experiment.getSize(), annotationLogger);

        if (annotationLogger != null) {
            annotationLogger
                    .log(new Action(millis(), Action.ANNOTATION_START, null));
        }
        ret.addSketchStoppedListener(this);

        return ret;
    }

    private Sketch createAnnotationSketch() {
        annotationLogger = createAnnotationLogger(
                AppConfig.General.ANNOTATION_SKETCH_ID);

        Sketch ret = new AnnotationSketch(this, getDocument(), annotationLogger);

        if (annotationLogger != null) {
            annotationLogger
                    .log(new Action(millis(), Action.ANNOTATION_START, null));
        }
        ret.addSketchStoppedListener(this);

        return ret;
    }

    private Mouse createMouse() {
        return new Mouse(millis(), mouseX, mouseY, mousePressed);
    }

    private void finishSuccessfully() {
        AnnotationLogger.writeMessageLogs(new LogPath(new ExperimentInfo(
                getUserName(), getExperiment(), getDocument(), -1)));

        StdLogger.getInstance().info("Successfully finished and exit.");
        System.exit(0);
    }

    public AnnotationLogger getAnnotationLogger() {
        return annotationLogger;
    }

    public Sketch getSketch() {
        return sketch;
    }

    private String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    private Document getDocument() {
        Document ret = null;
        if (documentIndex < experiment.getSize()) {
            ret = experiment.getDocument(documentIndex);
        }
        return ret;
    }

    private Experiment getExperiment() {
        return experiment;
    }

    public void setExperiment(Experiment experiment) {
        this.experiment = experiment;
    }

    public ControlP5 getCP5() {
        return cp5;
    }

    public static void main(String[] args) {
        PApplet.main(new String[] { "--present",
                "titech.segselect.SegSelect" });
    }
}
