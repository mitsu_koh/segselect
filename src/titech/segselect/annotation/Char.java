package titech.segselect.annotation;

import titech.segselect.experiment.AppConfig;

public class Char {
    char c;
    int offset;
    int parOffset;
    int lineOffset;

    public Char(char c, int i, int parOffset) {
        this.offset = i;
        this.parOffset = parOffset;
        this.c = c;
        this.lineOffset = -1;
    }

    public Char(char c, int i, int parOffset, int lineOffset) {
        this(c, i, parOffset);
        this.lineOffset = lineOffset;
    }

    public boolean isNewLine() {
        return String.valueOf(c).equals(AppConfig.Document.NEW_LINE);
    }

    public char getC() {
        return c;
    }

    public int getOffset() {
        return offset;
    }

    public int getParOffset() {
        return parOffset;
    }

    public int getLineOffset() {
        return lineOffset;
    }
}
