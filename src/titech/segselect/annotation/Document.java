package titech.segselect.annotation;

import java.util.ArrayList;

import titech.segselect.experiment.AppConfig;

public class Document {
    private String path;
    private String name;
    private ArrayList<Char> chars;
    private ArrayList<Segment> segments;

    public Document(String path, String name) {
        this.path = path;
        this.name = name;
        this.chars = new ArrayList<Char>();
        this.segments = new ArrayList<Segment>();
    }

    public Document(String path, String name, ArrayList<String> lines) {
        this(path, name);

        int i;

        int offset = 0;
        for (i = 0; i < lines.size(); i++) {
            if (lines.get(i).equals(AppConfig.Document.DELIMITER))
                break;
            int parOffset = 0;

            // If document file doesn't have new line adjustment
            // for (char c : (lines.get(i) + "\n").toCharArray()) {
            //     chars.add(new Char(c, offset, parOffset));
            //     offset++;
            //     parOffset++;
            // }

            // If document file has new line adjustment
            for (char c : lines.get(i).toCharArray()) {
                chars.add(new Char(c, offset, parOffset, i));
                offset++;
                parOffset++;
            }
        }

        // Skip Config.Format.DOC_DELIMITER line
        i++;

        for (; i < lines.size(); i++) {
            segments.add(new Segment(lines.get(i)));
        }
    }

    public String getPath() {
        return path;
    }

    public String getName() {
        return name;
    }

    public ArrayList<Char> getChars() {
        return chars;
    }

    public ArrayList<Segment> getSegments() {
        return segments;
    }
}