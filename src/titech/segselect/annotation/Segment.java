package titech.segselect.annotation;

public class Segment {
    public static String NOUN = "noun";
    public static String PRED = "pred";

    private int id;
    private int start;
    private int end;
    private String name;
    private String body;
    private String note;

    public Segment() {
        id = -1;
        start = -1;
        end = -1;
        name = "";
        body = "";
        note = "";
    }

    public Segment(String s) {
        String[] elms = s.split(",");
        id    = Integer.valueOf(elms[0]);
        start = Integer.valueOf(elms[1]);
        end   = Integer.valueOf(elms[2]);
        name  = elms[3];
        body  = elms[4];
        note  = elms[5];
    }

    public String toString() {
        if (id == -1) {
            return "*,*,*,*,*,*";
        } else {
            return id + "," + start + "," + end + "," + name + "," + body + "," + note;
        }
    }

    public int getId() {
        return id;
    }

    public int getStart() {
        return start;
    }

    public int getEnd() {
        return end;
    }

    public String getName() {
        return name;
    }

    public String getCont() {
        return body;
    }

    public String getNote() {
        return note;
    }
}